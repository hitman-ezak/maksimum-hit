$(document).ready(function(){
 $('a[name=modal]').click(function(e) {
  e.preventDefault();
  var id = $(this).attr('href');
  $(id).css('top', '-1000px');
  var maskHeight = $(document).height();
  var maskWidth = $(window).width();
  $('#mask').css({'width':maskWidth,'height':maskHeight});
  $('#mask').fadeIn(100);
  var winH = $(window).height();
  var winW = $(window).width();
  $(id).css('display', 'block');
  $(id).animate({"top":"100px"}, 300);
  $(id).css('left', winW/2-$(id).width()/2);
 });
 $('#form-for-reviews-horder .close').click(function (e) {
  e.preventDefault();
  $('#form-for-reviews-horder').css('top', '-1000px');
  $('#mask, #form-for-reviews-horder').hide();
 });
 $('#mask').click(function () {
  $(this).hide();
  $('#form-for-reviews-horder').css('top', '-1000px');
  $('#form-for-reviews-horder').hide();
 });
});


$(document).ready(function(){
 $('a[name=modal]').click(function(e) {
  e.preventDefault();
  var id = $(this).attr('href');
  $(id).css('top', '-1000px');
  var maskHeight = $(document).height();
  var maskWidth = $(window).width();
  $('#mask').css({'width':maskWidth,'height':maskHeight});
  $('#mask').fadeIn(300);
  var winH = $(window).height();
  var winW = $(window).width();
  $(id).css('display', 'block');
  $(id).animate({"top":"100px"}, 300);
  $(id).css('left', winW/2-$(id).width()/2);
 });
 $('#form-for-reviews-horder-second .close').click(function (e) {
  e.preventDefault();
  $('#form-for-reviews-horder-second').css('top', '-1000px');
  $('#mask, #form-for-reviews-horder-second').hide();
 });
 $('#mask').click(function () {
  $(this).hide();
  $('#form-for-reviews-horder-second').css('top', '-1000px');
  $('#form-for-reviews-horder-second').hide();
 });
});