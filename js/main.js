jQuery(function($){
    var images = [];
    
    $('.slider-mini-wrapper ul li a').each(function(e,i){
      var image = {
        href: $(this).attr('href'),
        title: 'Фотография '+(e+1)
      };
      
      images.push(image);
    })
    
    $(".fancybox").click(function(){
      $.fancybox.open(images, {
          padding : 0
      });
    });
    
    var dropZone = $('#dropZone'),
        maxFileSize = 1000000; // максимальный размер файла - 1 мб.

    if(dropZone.length) {
            
        if (typeof(window.FileReader) == 'undefined') {
            dropZone.text('Не поддерживается браузером!');
            dropZone.addClass('error');
        }
        
        dropZone[0].ondragover = function() {
            dropZone.addClass('hover');
            return false;
        };
            
        dropZone[0].ondragleave = function() {
            dropZone.removeClass('hover');
            return false;
        };

        dropZone[0].ondrop = function(event) {
            event.preventDefault();
            dropZone.removeClass('hover');
            dropZone.addClass('drop');
        };
        
        var file = event.dataTransfer.files[0];
            
        if (file.size > maxFileSize) {
            dropZone.text('Файл слишком большой!');
            dropZone.addClass('error');
            return false;
        }

        // xhr send file...
        
        function uploadProgress(event) {
            var percent = parseInt(event.loaded / event.total * 100);
            dropZone.text('Загрузка: ' + percent + '%');
        }
        
        function stateChange(event) {
            if (event.target.readyState == 4) {
                if (event.target.status == 200) {
                    dropZone.text('Загрузка успешно завершена!');
                } else {
                    dropZone.text('Произошла ошибка!');
                    dropZone.addClass('error');
                }
            }
        }
    }

    $('.rating').each(function(){
      var self = $(this);
      var rating = $(this).data('rating');
      for(i=0;i<=rating;i++) {
        self.find('a:eq('+i+'),span:eq('+i+')').addClass('active');
      }
    })

    var maskList = $.masksSort([{"mask": "+7(###)###-##-##"}], ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            showMaskOnHover: true,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    $('#your-phone').change(function() {
        $('#your-phone').inputmasks(maskOpts);
    });

    $('#your-phone').change();
})