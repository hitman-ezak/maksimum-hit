<?php
get_header();
global $wpdb;
$queried_object = get_queried_object(); 
$taxonomy = $queried_object->taxonomy;
$term_id = $queried_object->term_id;  
$current_cat = get_category($term_id);
$count_posts = $current_cat->category_count;
$nr = $count_posts;
if (isset($_GET['pn'])) {
    $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
} else {
    $pn = 1;
}
$options = get_option('maksimum_options');
$itemsPerPage = $options['image_number'];
$lastPage = ceil($nr / $itemsPerPage);
if ($pn < 1) {
    $pn = 1;
} else if ($pn > $lastPage) {
    $pn = $lastPage;
}
$limit = 'LIMIT ' .($pn - 1) * $itemsPerPage .',' .$itemsPerPage;
$girls = $wpdb->get_results( 
	"
	SELECT *
	FROM $wpdb->posts wposts
            LEFT JOIN $wpdb->term_relationships ON (wposts.ID = $wpdb->term_relationships.object_id)
            LEFT JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
	WHERE post_status = 'publish' 
                AND $wpdb->term_taxonomy.taxonomy = 'category'
                AND $wpdb->term_taxonomy.term_id IN($term_id)
		AND post_type = 'girls'
                $limit
	"
);
?>
<?php
$commenter = wp_get_current_commenter();
$fields =  array(
    'author' => '<div class="row"><input id="name" type="text" name="author" placeholder="Имя*" class="required" /></div>',
    'captcha' => '<div class="captcha-holder">						
            <script type="text/javascript"
               src="https://www.google.com/recaptcha/api/challenge?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm">
            </script>
            <noscript>
               <iframe src="https://www.google.com/recaptcha/api/noscript?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm"
                   height="300" width="500" frameborder="0"></iframe><br>
               <textarea name="recaptcha_challenge_field" rows="3" cols="40">
               </textarea>
               <input type="hidden" name="recaptcha_response_field"
                   value="manual_challenge">
            </noscript>
                    </div>',
    'rating' => '<input name="rating" value="0" type="hidden" />'
);
 
$comments_args = array(
    'comment_field' => '<div class="row"><textarea id="comments" cols="30" rows="5" name="comment" placeholder="Комментарий*" class="required"></textarea></div>',
    'fields' =>  $fields,
    'title_reply'=>'',
    'id_form' => 'form-reviews',
    'label_submit' => 'Отправить',
    'comment_notes_after' => '',
    'comment_notes_before' => ''
    
);
 

?>

<div id="openModalreviews" class="form-for-reviews-holder">
    <div class="box">
        <div id="form-for-reviews-horder">
            <div class="what-to-do-holder">
                <div class="title">
                    <h2>Оставить отзыв</h2>
                    <a href="#" class="close"></a>
                </div>
            <div class="rating-holder">
                <div class="what-to-do" style="display: none"></div>
                <span>Ваша оценка *</span>
                <div class="rating comment">
                    <ul>
                        <li><a id="one" title="Плохо" href="javascript"></a></li>
                        <li><a id="two" title="Приемлемо" href="#"></a></li>
                        <li><a id="three" title="Средне" href="#"></a></li>
                        <li><a id="four" title="Хорошо" href="#"></a></li>
                        <li><a id="five" title="Отлично" href="#"></a></li>
                    </ul>
                </div>
            </div>
            <?php comment_form($comments_args,1)?>
            </div>
        </div>
        <div id="mask"></div>
    </div>
</div>
<div id="page_main">
    <?php if ( $girls ) : ?>
        <?php foreach ( $girls as $girl ): ?>
    <?php
    $args = array(
	'status' => 'approve',
	'post_id' => $girl->ID
        );
        $comments = get_comments($args);
        $count = 0;
        $all_ranks = 0;
        foreach($comments as $comment) :
                $all_ranks += get_comment_meta( $comment->comment_ID, 'rating', true );
                $count++;
        endforeach;
        if ($count>0):
            $rank = $all_ranks/$count;
        else:
            $rank = 0;
        endif;
        //global $withcomments; $withcomments = true;
        //comments_template();
?>
        <?php if ( $images = get_posts( array(
                'post_parent' => $girl->ID,
                'post_type' => 'attachment',
                'numberposts' => -1,
                'orderby' => 'title',
                'order' => 'ASC',
                'post_mime_type' => 'image',
                'exclude' => $thumb_ID,
            )))
                {
                        $small_image_url = wp_get_attachment_image($images[0]->ID, 'archive-profile');
                }?>
            <div class="model">
                <div class="model-info">
                    <div class="title">
                        <a href="<?php echo get_permalink($girl->ID)?>" target="_blank"><h3><?php echo $girl->post_title;?></h3></a>
                        <div class="rating" data-rating="<?php echo round($rank);?>">
                            <ul>
                                <li><a title="Плохо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Приемлемо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Средне" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Хорошо" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                                <li><a title="Отлично" rel="<?php echo $girl->ID;?>" name="modal" href="#form-for-reviews-horder"></a></li>
                            </ul>
 			</div>
                    </div>
                    <p><?php echo $girl->post_excerpt?></p>
                    <span>Тел. <?php echo get_post_meta($girl->ID, "phone", true)?></span>
                    <a href="<?php echo get_permalink($girl->ID)?>" target="_blank">Читать далее</a>
                </div>
                <div class="model-photo-holder">
                    <div class="model-photo">
                        <a class="model-photo" href="<?php echo get_permalink($girl->ID)?>" target="_blank">
                            <?php if (isset($images[0])){ echo $small_image_url;}?>
                        </a>
                        <!--img class="biggg" src="<?php echo $large_image_url[0]?>" alt="<?php echo the_title_attribute('echo=0')?>"-->
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php echo getPaginationString($pn, $nr, $itemsPerPage, $adjacents = 1, $targetpage = " ", $pagestring = "?pn=");?>
    <?php else : ?>
        <?php echo 'Анкет не найдено.'?>
    <?php endif; ?>
</div>
<?php get_footer(); ?>