﻿		</div>
		<?php
		$post = $wp_query->get_queried_object();
//		$postType = $post->post_type;
		?>
		<?php $options = get_option('maksimum_options');?>
		<?php $excludeArray = array('faq', 'submit', 'rules');?>

		<?php if( !in_array($post->post_name, $excludeArray)
			AND $options['banner_left']!= '' || $options['banner_right']!= ''){?>
			<div class="banners">
				<?php if($options['banner_left']!= ''){?>
					<div class="left-b">
						<?php echo $options['banner_left']?>
					</div>
				<?php } ?>
				<?php if($options['banner_right']!= ''){?>
					<div class="right-b">
						<?php echo $options['banner_right']?>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
	<!-- footer -->
	<div id="footer">
		<div class="footer-holder">
			<ul class="footer-nav">
				<div class="footer-nav-part">
					<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'footer-left' ) ); ?>
				</div>
				<div class="footer-nav-part">
					<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'footer-right' ) ); ?>
				</div>
			</ul>
			<div class="footer-bottom">

                <!--LiveInternet counter--><script type="text/javascript"><!--
                    document.write("<a href='http://www.liveinternet.ru/click' "+
                        "target=_blank><img src='//counter.yadro.ru/hit?t52.1;r"+
                        escape(document.referrer)+((typeof(screen)=="undefined")?"":
                        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
                        ";"+Math.random()+
                        "' alt='' title='LiveInternet: показано число просмотров и"+
                        " посетителей за 24 часа' "+
                        "border='0' width='88' height='31'><\/a>")
                    //--></script><!--/LiveInternet-->


                <span class="copyrigth">2013-2014 &copy; intimbaza.nl</span>
			</div>
		</div>
		
	</div>
        <?php wp_footer(); ?>
</body>
</html>