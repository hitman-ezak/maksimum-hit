<?php
session_start();
require get_template_directory() . '/inc/custom-header.php';

function maksimum_setup() {

	load_theme_textdomain( 'twentythirteen', get_template_directory() . '/languages' );

	add_editor_style( array( 'css/editor-style.css', 'fonts/genericons.css', maksimum_fonts_url() ) );

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );


	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 183, 200, true );

	add_filter( 'use_default_gallery_style', '__return_false' );
}
add_action( 'after_setup_theme', 'maksimum_setup' );

function maksimum_fonts_url() {
	$fonts_url = '';

	$source_sans_pro = _x( 'on', 'Source Sans Pro font: on or off', 'twentythirteen' );

	$bitter = _x( 'on', 'Bitter font: on or off', 'twentythirteen' );

	if ( 'off' !== $source_sans_pro || 'off' !== $bitter ) {
		$font_families = array();

		if ( 'off' !== $source_sans_pro )
			$font_families[] = 'Source Sans Pro:300,400,700,300italic,400italic,700italic';

		if ( 'off' !== $bitter )
			$font_families[] = 'Bitter:400,700';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$fonts_url = add_query_arg( $query_args, "//fonts.googleapis.com/css" );
	}

	return $fonts_url;
}

function maksimum_scripts_styles() {

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	if ( is_active_sidebar( 'sidebar-1' ) )
		wp_enqueue_script( 'jquery-masonry' );

	//wp_enqueue_script( 'maksimum-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '2013-07-18', true );
	//wp_enqueue_script( 'maksimum-script', get_template_directory_uri() . '/js/hover.js', array(), '2013-07-18', false );

	wp_enqueue_style( 'maksimum-style', get_stylesheet_uri(), array(), '2013-07-18' );
}
add_action( 'wp_enqueue_scripts', 'maksimum_scripts_styles' );

function maksimum_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	$title .= get_bloginfo( 'name' );

	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";


	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'maksimum' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'maksimum_wp_title', 10, 2 );

function maksimum_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area', 'maksimum' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the footer section of the site.', 'maksimum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Secondary Widget Area', 'maksimum' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears on posts and pages in the sidebar.', 'maksimum' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'maksimum_widgets_init' );

if ( ! function_exists( 'maksimum_paging_nav' ) ) :

function maksimum_paging_nav() {
	global $wp_query;

	if ( $wp_query->max_num_pages < 2 )
		return;
	?>
	<nav class="navigation paging-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'maksimum' ); ?></h1>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'maksimum' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'maksimum' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'maksimum_post_nav' ) ) :

function maksimum_post_nav() {
	global $post;

	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous )
		return;
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Post navigation', 'maksimum' ); ?></h1>
		<div class="nav-links">

			<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', 'maksimum' ) ); ?>
			<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', 'maksimum' ) ); ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'maksimum_entry_meta' ) ) :
function maksimum_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'maksimum' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		twentythirteen_entry_date();


	$categories_list = get_the_category_list( __( ', ', 'maksimum' ) );
	if ( $categories_list ) {
		echo '<span class="categories-links">' . $categories_list . '</span>';
	}


	$tag_list = get_the_tag_list( '', __( ', ', 'maksimum' ) );
	if ( $tag_list ) {
		echo '<span class="tags-links">' . $tag_list . '</span>';
	}


	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'maksimum' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'maksimum_entry_date' ) ) :

function maksimum_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'maksimum' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'maksimum' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}
endif;

if ( ! function_exists( 'maksimum_the_attached_image' ) ) :

function maksimum_the_attached_image() {
	$post                = get_post();
	$attachment_size     = apply_filters( 'maksimum_attachment_size', array( 724, 724 ) );
	$next_attachment_url = wp_get_attachment_url();

	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID'
	) );

	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		if ( $next_id )
			$next_attachment_url = get_attachment_link( $next_id );

		else
			$next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
	}

	printf( '<a href="%1$s" title="%2$s" rel="attachment">%3$s</a>',
		esc_url( $next_attachment_url ),
		the_title_attribute( array( 'echo' => false ) ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;

function maksimum_get_link_url() {
	$content = get_the_content();
	$has_url = get_url_in_content( $content );

	return ( $has_url ) ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}

function maksimum_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	if ( ! get_option( 'show_avatars' ) )
		$classes[] = 'no-avatars';

	return $classes;
}
add_filter( 'body_class', 'maksimum_body_class' );

function maksimum_content_width() {
	global $content_width;

	if ( is_attachment() )
		$content_width = 724;
	elseif ( has_post_format( 'audio' ) )
		$content_width = 484;
}
add_action( 'template_redirect', 'maksimum_content_width' );

function maksimum_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'maksimum_customize_register' );


register_nav_menus(array(  
    'header' => 'Верхнее меню шапки',
	'footer-left' => 'Левое меню подвала',
    'footer-right' => 'Правое меню подвала'
));  

add_action('init', 'faq_init');

function faq_init() {
$args = array(
		'label' => __('FAQ'),
		'labels' => array(
				'all_items' => __('Все FAQ'),
				'edit_item' => __('Редактировать FAQ'),
				'add_new_item' => __('Добавить новый FAQ'),
				'view_item' => __('Показать FAQ'),
				),
		'singular_label' => __('FAQ'),
		'public' => true,
		'show_ui' => true,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => true,
		'rewrite' => array("slug" => "faq"),
		'supports' => array('title', 'editor', 'thumbnail')
		);
register_post_type( 'faq' , $args );
}

add_action('init', 'girls_init');

function girls_init() {
$args = array(
		'label' => __('Анкеты'),
		'labels' => array(
				'all_items' => __('Все Анкеты'),
				'edit_item' => __('Редактировать Анкету'),
				'add_new_item' => __('Добавить новую Анкету'),
				'view_item' => __('Показать Анкету'),
				),
                'singular_label' => __('Анкеты'),
		'public' => true,
		'show_ui' => true,
                'publicly_queryable' => true,
                'exclude_from_search' => false,
		'_builtin' => false,
		'_edit_link' => 'post.php?post=%d',
		'capability_type' => 'post',
		'hierarchical' => true,
                'query_var' => true,
                'has_archive' => 'girls',
		'rewrite' => array("slug" => "girls"),
                'taxonomies' => array('category'),
		'supports' => array('title', 'editor', 'thumbnail','page-attributes','comments','custom-fields','excerpt')
		);
register_post_type( 'girls' , $args );
}


add_action("pre_get_posts", "custom_front_page");
function custom_front_page($wp_query){
    if(is_admin()) return;
    if($wp_query->get('page_id') == get_option('page_on_front')):

        $wp_query->set('post_type', 'girls');
        $wp_query->set('page_id', ''); 

        $wp_query->is_page = 0;
        $wp_query->is_singular = 0;
        $wp_query->is_post_type_archive = 1;
        $wp_query->is_archive = 1;
    endif;
}

//comments

function remove_comment_fields($fields) {
    unset($fields['url']);
    unset($fields['email']);
    return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');

add_filter('preprocess_comment', 'require_comment_author');

function require_comment_author($commentdata) {

    if ('' == $commentdata['comment_author'])
        wp_die('<strong>ОШИБКА</strong>: пожалуйста, введите имя.');
    return $commentdata;
}
function add_comment_meta_values($comment_id) {
 
    if(isset($_POST['rating'])) {
        $rating = wp_filter_nohtml_kses($_POST['rating']);
        add_comment_meta($comment_id, 'rating', $rating, false);
    }
 
}
add_action ('comment_post', 'add_comment_meta_values', 1);

function maksimum_comments_callback( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
 
    ?>
        <div class="reviews-post">
            <div class="title">
                <h5><?php comment_author(); ?></h5>
                <span><?php comment_date('d.m.Y'); ?></span>
                <div class="rating"  data-rating="<?php echo get_comment_meta( $comment->comment_ID, 'rating', true ) ?>">
                    <ul>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                        <li><span></span></li>
                    </ul>
                </div>
            </div>
            <p><?php comment_text(); ?></p>
        </div>
    <?php
}

function set_js_var() {
  $options = get_option('maksimum_options');
  $images_number = $options['image_number'];
  $translation_array = array( 'template_directory_uri' => get_template_directory_uri(), 'images_number'=> $images_number);
  wp_localize_script( 'jquery', 'my_data', $translation_array );
}
add_action('wp_enqueue_scripts','set_js_var');

/*function plu_enqueue() {
    wp_enqueue_script('plupload-all');
 
    wp_register_script('myplupload', get_template_directory_uri().'/js/uploader/myplupload.js', array('jquery', 'jquery-ui-sortable'));
    wp_enqueue_script('myplupload');
}
add_action( 'wp_enqueue_scripts', 'plu_enqueue' );
function g_plupload_action() {
 
    // check ajax noonce
    $imgid = $_POST["imgid"];
    check_ajax_referer($imgid . 'pluploadan');
 
    // handle file upload
    $status = wp_handle_upload($_FILES[$imgid . 'async-upload'], array('test_form' => true, 'action' => 'plupload_action'));
 
    // send the uploaded file url in response
    echo $status['url'];
    exit;
}
add_action('wp_ajax_plupload_action', "g_plupload_action");*/


add_image_size('profile-slider', 181, 198, true);
add_image_size('profile', 458, 602, true);
add_image_size('archive-profile', 183, 200, true);

function randomise_with_pagination( $result ) {

    if( is_archive() ) {

		// Reset seed on load of initial archive page
                
		if( ! get_query_var( 'page' ) || get_query_var( 'page' ) == 0 || get_query_var( 'page' ) == 1 ) {
			if( isset( $_SESSION['seed'] ) ) {
				unset( $_SESSION['seed'] );
			}
		}

		// Get seed from session variable if it exists
		$seed = false;
		if( isset( $_SESSION['seed'] ) ) {
	    	$seed = $_SESSION['seed'];
	    }

	    // Set new seed if none exists
	    if ( ! $seed ) {
	        $seed = rand();
	        $_SESSION['seed'] = $seed;
                
	    }
            $result = 'RAND('.$seed.')';
	}

    return $result;
}
function maximum_home_pagesize( $query ) {
    global $wp_query;
    if ( is_admin() || ! $query->is_main_query() )
        return;
    if (is_archive() || is_category()) {
            //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
            $options = get_option('maksimum_options');
            //$query->set('posts_per_page', $options['image_number']);
            $query->set('post_type','girls');
            $query->set('post_status','publish');
            //$query->set('paged',$paged);
            add_filter( 'posts_orderby', 'randomise_with_pagination' );
            //var_dump($query->max_num_pages);
            //echo "DEBUG: is_archive() <pre>".print_r($query,true)."</pre>";
        return $query;
    }
}
//add_action( 'pre_get_posts', 'maximum_home_pagesize');
function maksimum_pagination()
{  
 global $wp_query;
 //echo "DEBUG: is_pag() <pre>".print_r($wp_query,true)."</pre>";
    $pages = '';
    $max = $wp_query->max_num_pages;
    //var_dump($wp_query->found_posts);
    //var_dump($wp_query->max_num_pages);
    if (!$current = get_query_var('page')) $current = 1;
    $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
    $a['total'] = $wp_query->max_num_pages;
    $a['current'] = $current;
 
    $total = 1; //1 - display the text "Page # of #", 0 - not display
    $a['mid_size'] = 2; //how many links to show on the left and right of the current
    $a['end_size'] = 1; //how many links to show in the beginning and end
    $a['type'] = 'list';
    $a['prev_text'] = ''; //text of the "Previous page" link
    $a['next_text'] = ''; //text of the "Next page" link
 
    if ($max > 1) echo '<div class="paging-holder">';
        echo paginate_links($a); 
    if ($max > 1) echo '</div>';
     
}
/*add_action('pre_get_posts', 'advanced_search_query');
function advanced_search_query($query) {
    if ( is_admin() || ! $query->is_main_query() )
        return;
    
    if(is_search()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
            var_dump($_GET['s']);
            $query->set('posts_per_page', 5);
            $query->set('post_type','girls');
            $query->set('post_status','publish');
            $query->set('paged',$paged);
            $query->set('meta_query', array(
                    array(
                          'key' => 'phone',
                          'value' => $_GET['s'],
                          'compare' => 'LIKE'
                    )
                ));
            
            //add_filter( 'posts_orderby', 'randomise_with_pagination' );
            //echo "DEBUG: is_archive() <pre>".print_r($query,true)."</pre>";
         return $query;
    }
 
}*/
//admin menu options create
function maksimum_register_settings() {
    register_setting( 'maksimum_theme_options', 'maksimum_options', 'maksimum_validate_options' );
}
 
add_action( 'admin_init', 'maksimum_register_settings' );
$settings = get_option('maksimum_options');
$maksimum_options = array(
    'mature' => true,
    'image_number' =>'',
    'header_text' => '',
	'banner_left' => '',
	'banner_right' => ''
);
$settings = get_option( 'maksimum_options', $maksimum_options );
function maksimum_theme_options() {
    add_theme_page( 'Общие настройки', 'Общие настройки', 'edit_theme_options', 'theme_options', 'maksimum_theme_options_page' );
}
 
add_action( 'admin_menu', 'maksimum_theme_options' );
function maksimum_theme_options_page() {
    global $maksimum_options;
 
    if ( ! isset( $_GET['settings-updated'] ) )
    $_GET['settings-updated'] = false;?>
 
    <div>
        <?php screen_icon(); echo "<h2>".__( 'Общие настройки')."</h2>";?>
        <?php if ( false !== $_GET['settings-updated'] ) : ?>
            <div class="updated below-h2" id="message"><p>Настройки обновлены.</p></div>
        <?php endif;?>
        <form method="post" action="options.php">
            <?php $settings = get_option( 'maksimum_options', $maksimum_options ); ?>
            <?php settings_fields( 'maksimum_theme_options' );?>
            <table>
                <tr valign="top"><td></br></br></td>
                <tr valign="top"><th style="float:left;" scope="row">Показывается всплывающее окно 18+</th>
                    <td>
                        <input type="checkbox" id="author_credits" style="margin-left:10px;" name="maksimum_options[mature]" value="1" <?php checked( true, $settings['mature'] ); ?> />
                        <label for="mature">показывать окно</label>
                    </td>
                </tr>
                <tr valign="top"><th style="float:left;" scope="row">Количество анкет на странице</th>
                    <td>
                        <input type="text" id="image_number" style="width:30px;margin-left:10px;" name="maksimum_options[image_number]" value="<?php  esc_attr_e($settings['image_number']); ?>" />
                    </td>
                </tr>
                <tr valign="top"><th style="float:left;" scope="row">Описание под логотипом</th>
                    <td>
                        <input type="text" id="header_text" style="width:300px;margin-left:10px;" name="maksimum_options[header_text]" value="<?php  esc_attr_e($settings['header_text']); ?>" />
                    </td>
                </tr>
				<tr valign="top"><th style="float:left;" scope="row">Код левого баннера (468x60)</th>
                    <td>
                        <textarea id="banner_left" style="width:300px;height:65px;margin-left:10px;" name="maksimum_options[banner_left]" value="<?php  esc_attr_e($settings['banner_left']); ?>"><?php  esc_attr_e($settings['banner_left']); ?></textarea>
                    </td>
                </tr>
				<tr valign="top"><th style="float:left;" scope="row">Код правого баннера (468x60)</th>
                    <td>
                        <textarea id="banner_right" style="width:300px;height:65px;margin-left:10px;" name="maksimum_options[banner_right]" value="<?php  esc_attr_e($settings['banner_right']); ?>"><?php  esc_attr_e($settings['banner_right']); ?></textarea>
                    </td>
                </tr>
            </table>
            <p><input type="submit" value="Сохранить" /></p>
        </form>
    </div>
 
    <?php
}
function maksimum_validate_options( $input ) {
    global $maksimum_options;
 
    $settings = get_option( 'maksimum_options', $maksimum_options );
    
    if (!is_numeric($input['image_number']))
    {
        $input['image_number'] = 0;
    } else {
        $input['image_number'] = wp_filter_post_kses( $input['image_number'] );
    }
    $input['header_text'] = wp_filter_post_kses( $input['header_text'] );
	$input['banner_left'] = $input['banner_left'];
	$input['banner_right'] = $input['banner_right'];
    if ( ! isset( $input['mature'] ) )
    $input['mature'] = null;
    $input['mature'] = ( $input['mature'] == 1 ? 1 : 0 );
     return $input;
}
global $maksimum_options;
$maksimum_settings = get_option( 'maksimum_options', $maksimum_options );

add_action('comment_post', 'ajaxify_comments',20, 2);
function ajaxify_comments($comment_ID, $comment_status){
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    //If AJAX Request Then
    switch($comment_status){
        case '0':
        //notify moderator of unapproved comment
        wp_notify_moderator($comment_ID);
        case '1': //Approved comment
            echo "success";
            $commentdata=&get_comment($comment_ID, ARRAY_A);
            $post=&get_post($commentdata['comment_post_ID']);
            //wp_notify_postauthor($comment_ID, $commentdata['comment_type']);
        break;
        default:
            echo "error";
    }
    exit;
    }
}

//function to return the pagination string
function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=")
{
	//defaults
	if(!$adjacents) $adjacents = 1;
	if(!$limit) $limit = 15;
	if(!$page) $page = 1;
	if(!$targetpage) $targetpage = "/";
	
	//other vars
	$prev = $page - 1;									//previous page is page - 1
	$next = $page + 1;									//next page is page + 1
	$lastpage = ceil($totalitems / $limit);				//lastpage is = total items / items per page, rounded up.
	$lpm1 = $lastpage - 1;								//last page minus 1
	
	/* 
		Now we apply our rules and draw the pagination object. 
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"paging-holder\"";
		if($margin || $padding)
		{
			$pagination .= " style=\"";
			if($margin)
				$pagination .= "margin: $margin;";
			if($padding)
				$pagination .= "padding: $padding;";
			$pagination .= "\"";
		}
		$pagination .= "><ul class=\"page-numbers\">";

		//previous button
		if ($page > 1) 
			$pagination .= "<li><a class=\"page-numbers prev\" href=\"$targetpage$pagestring$prev\"></a></li>";
		//else
			//$pagination .= "<span class=\"disabled\">« prev</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination .= "<li><span class=\"page-numbers current\">$counter</span></li>";
				else
					$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a></li>";					
			}
		}
		elseif($lastpage >= 7 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 3))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination .= "<li><span class=\"page-numbers current\">$counter</span></li>";
					else
						$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a></li>";					
				}
				$pagination .= "<li><span class=\"elipses\">...</span></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a></li>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . "1\">1</a></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . "2\">2</a></li>";
				$pagination .= "<li><span class=\"elipses\">...</span></li>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<li><span class=\"page-numbers current\">$counter</span></li>";
					else
						$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a></li>";					
				}
				$pagination .= "<li><span>...</span></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a></li>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . "1\">1</a></li>";
				$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . "2\">2</a></li>";
				$pagination .= "<li><span class=\"elipses\">...</span></li>";
				for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<li><span class=\"page-numbers current\">$counter</span></li>";
					else
						$pagination .= "<li><a class=\"page-numbers\" href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a></li>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination .= "<li><a class=\"page-numbers next\" href=\"" . $targetpage . $pagestring . $next . "\"></a></li>";
		//else
			//$pagination .= "<span class=\"disabled\">next »</span>";
		$pagination .= "</ul></div>\n";
	}
	
	return $pagination;

}
function remove_admin_menu_items() {
    remove_menu_page('link-manager.php');
    remove_menu_page('edit.php');
    remove_menu_page('index.php');
    remove_menu_page('plugins.php');
    remove_menu_page('users.php');
    remove_menu_page('tools.php');
    remove_menu_page('options-general.php');
    remove_menu_page('edit.php?post_type=acf');
    remove_menu_page('edit.php?post_type=page');
    remove_menu_page('edit.php?post_type=faq');
    remove_menu_page('upload.php');
    remove_menu_page('profile.php');
    remove_submenu_page('post-new.php', 'post-new.php?post_type=faq');
    remove_submenu_page('edit.php?post_type=girls', 'edit-tags.php?taxonomy=category&amp;post_type=girls');
    
    remove_menu_page('admin.php?page=w3tc_dashboard');
	remove_submenu_page('options-general.php', 'options-writing.php');
	remove_submenu_page('options-general.php', 'options-reading.php');
	remove_submenu_page('options-general.php', 'options-discussion.php');
	remove_submenu_page('options-general.php', 'options-media.php');
	remove_submenu_page('options-general.php', 'options-permalink.php');
	remove_menu_page('post-expirator.php');
	
    remove_submenu_page( 'themes.php', 'themes.php' );
    remove_submenu_page( 'themes.php', 'widgets.php' );
    remove_submenu_page( 'themes.php', 'nav-menus.php' );
    remove_submenu_page( 'themes.php', 'theme-editor.php' );
    remove_submenu_page( 'themes.php', 'themes.php?page=custom-header' );
}
function get_user_role() {
	global $wp_roles;
        
        $current_user = wp_get_current_user();
	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);
        if ($user_role == 'editor'){
            add_action( 'admin_menu', 'remove_admin_menu_items' );
            add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
        }
}

function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('new-post');
    $wp_admin_bar->remove_menu('new-media');
    $wp_admin_bar->remove_menu('new-page');
    $wp_admin_bar->remove_menu('new-faq');
}

add_action( 'after_setup_theme','get_user_role' );
add_action( 'after_setup_theme','remove_twentyeleven_options', 100 );
function remove_twentyeleven_options() {
    remove_custom_image_header();
}

//Pre-select post specific attachments

add_action( 'admin_footer-post-new.php', 'wpse_76048_script' );
add_action( 'admin_footer-post.php', 'wpse_76048_script' );

function wpse_76048_script()
{
    ?>
<script>
jQuery(function($) {
    wp.media.view.UploaderWindow.prototype.on('ready',function() {console.log(jQuery('.media-modal-content'))
                $('[value="uploaded"]').attr( 'selected', true ).parent().trigger('change');
            });
  var oldPost = wp.media.view.MediaFrame.Post;
    wp.media.view.MediaFrame.Post = oldPost.extend({
        initialize: function() {
            oldPost.prototype.initialize.apply( this, arguments );
            this.states.get('insert').get('library').props.set('uploadedTo', wp.media.view.settings.post.id);
        }
    });
});
</script>
    <?php
}
add_filter( 'media_view_strings', 'cor_media_view_strings' );
/**
 * @see wp-includes|media.php
 */
function cor_media_view_strings( $strings ) {
    unset( $strings['setFeaturedImageTitle'] );
    return $strings;
}

function my_custom_post_status(){
	register_post_status( 'draft', array(
		'label'                     => _x( 'Неоплаченные', 'post' ),
		'public'                    => true,
		'exclude_from_search'       => false,
		'show_in_admin_all_list'    => true,
		'show_in_admin_status_list' => true,
		'label_count'               => _n_noop( 'Неоплаченные <span class="count">(%s)</span>', 'Неоплаченные <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'my_custom_post_status' );
function display_draft_state( $states ) {
     global $post;
     $arg = get_query_var( 'post_status' );
     if($arg != 'draft'){
          if($post->post_status == 'draft'){
               return array('Неоплаченные');
          }
     }
    return $states;
}
add_filter( 'display_post_states', 'display_draft_state' );


add_action( 'admin_footer-post-new.php', 'wpse_1111_script' );
add_action( 'admin_footer-post.php', 'wpse_1111_script' );
//switch places of content and excerpt
function wpse_1111_script()
{
    ?>
<script>
jQuery(function($) {
	var $excerpt	= $('#postexcerpt');
	var $wysiwyg	= $('#postdivrich');

	$wysiwyg.prepend($excerpt);
});
</script>
<?php
}
global $postmeta_alias, $is_specials_search;
$cpt_name = 'girls';
$postmeta_alias = 'girls'; // Change this to whatever your custom post type is
$is_specials_search = is_admin() && $pagenow=='edit.php' && isset( $_GET['post_type'] ) && $_GET['post_type']==$cpt_name && isset( $_GET['s'] );

// Extend search to include 'phone' field
if ( $is_specials_search ) {
  add_filter( 'posts_join',      'pd_description_search_join' );
  add_filter( 'posts_where',     'pd_description_search_where' );
  add_filter( 'posts_groupby',   'pd_search_dupe_fix' );
}

function pd_description_search_join ($join){
  global $pagenow, $wpdb, $postmeta_alias, $is_specials_search;

  if ( $is_specials_search )  
    $join .='LEFT JOIN '.$wpdb->postmeta. ' as ' . $postmeta_alias . ' ON '. $wpdb->posts . '.ID = ' . $postmeta_alias . '.post_id ';
  
    /*echo '<br><strong>JOIN</strong>: ';
    print_r ( $join );
    echo '<br>';*/

  return $join;
} // END search_join

function pd_description_search_where( $where ){
  global $pagenow, $wpdb, $postmeta_alias, $is_specials_search;

  if ( $is_specials_search )
    $where = preg_replace(
     "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
     "(".$wpdb->posts.".post_title LIKE $1) OR ((".$postmeta_alias.".meta_value LIKE $1) AND (".$postmeta_alias.".meta_key = 'phone'))", $where );
  
    /*echo '<br><strong>WHERE</strong>: ';
    print_r ( $where );
    echo '<br>';*/
  return $where;
} // END search_where

function pd_search_dupe_fix($groupby) {
    global $pagenow, $wpdb, $is_specials_search;

    if ( $is_specials_search )
      $groupby = "$wpdb->posts.ID";

    return $groupby;
} // END pd_search_dupe_fix
function replace_http( $original ) {
    // use preg_replace() to replace http:// with https://
    $output = preg_replace( "^http:", "https:", $original );
    return $output;
}

add_filter( 'template_url', 'replace_http' );