function noCrime() {
    $('img').on('dragstart', function(event) { event.preventDefault(); });
    $('img').on('onmousedown ', function(event) { event.preventDefault(); });
    $('img').bind('contextmenu', function(e) {
        return false;
    });
}

jQuery(function($){
    noCrime();

// disable image dragging
    function disableDragging(e) {
        e.preventDefault();
    }

	var fancy = $('a.fancybox');
	if(fancy.length) {
		jQuery("a.fancybox").fancybox({
			padding : 0
			/*afterLoad: function(){
				this.title = this.title + ' ' + $(this.element).find('img').attr('alt');
			}*/
		});
	}

    /*var images = [];
    
    $('.slider-mini-wrapper ul li a').each(function(e,i){
      var image = {
        href: $(this).attr('href'),
        title: 'Фотография '+(e+1)
      };
      
      images.push(image);
    })
    
    $(".fancybox").click(function(){
      jQuery.fancybox.open(images, {
          padding : 0
      });
    });*/
    
    var dropZone = $('#dropZone')
        //maxFileSize = 1000000; // максимальный размер файла - 1 мб.

    if(dropZone.length) {
            
        if (typeof(window.FileReader) == 'undefined') {
            dropZone.text('Не поддерживается браузером!');
            dropZone.addClass('error');
        }
        
        dropZone[0].ondragover = function() {
            dropZone.addClass('hover');
            return false;
        };
            
        dropZone[0].ondragleave = function() {
            dropZone.removeClass('hover');
            return false;
        };

        dropZone[0].ondrop = function(event) {
            event.preventDefault();
            dropZone.removeClass('hover');
            dropZone.addClass('drop');
        };
        
//        var file = event.dataTransfer.files[0];
//            
//        if (file.size > maxFileSize) {
//            dropZone.text('Файл слишком большой!');
//            dropZone.addClass('error');
//            return false;
//        }
//
//        // xhr send file...
//        
//        function uploadProgress(event) {
//            var percent = parseInt(event.loaded / event.total * 100);
//            dropZone.text('Загрузка: ' + percent + '%');
//        }
//        
//        function stateChange(event) {
//            if (event.target.readyState == 4) {
//                if (event.target.status == 200) {
//                    dropZone.text('Загрузка успешно завершена!');
//                } else {
//                    dropZone.text('Произошла ошибка!');
//                    dropZone.addClass('error');
//                }
//            }
//        }
    }

    $('.rating').each(function(){
      var self = $(this);
      var rating = $(this).data('rating');
      for(i=0;i<rating;i++) {
        self.find('a:eq('+i+'),span:eq('+i+')').addClass('active');
      }
    })

    /*var maskList = $.masksSort([{"mask": "+3##(##)###-##-##"}], ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            showMaskOnHover: true,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function(maskObj, completed) {
            $(this).inputmask("+999(99)999-99-99");//getemptymask in inputmask
        }
    };

    $('#your-phone').change(function() {
        $('#your-phone').inputmasks(maskOpts);
    });

    $('#your-phone').change();
    
    $('#postPhone').change(function() {
        $('#postPhone').inputmasks(maskOpts);
    });

    $('#postPhone').change();
    $("#postTitle").inputmask('Regex', { regex: "[A-Za-z\u0410-\u044F\u0401\u0451 !.,-]*"});
    $("#name").inputmask('Regex', { regex: "[A-Za-z\u0410-\u044F\u0401\u0451 !.,-]*"});
          
            $(".btn-search" ).click(function() {
                if ($('#your-phone').val()==''){
                        return false;
                    } else {
                        $( ".search-form" ).submit();
                        return false;
                    }
            });      
            
            $("#submit" ).click(function() {
                var action = $("#profile").val();
                $("#profile-form").attr("action", "/submit/" + action);
                $("#profile-form").submit();
                return false;
            });
            
            $("#profile-form").validate({
                onfocusout: false,
                onkeyup:false,
                errorLabelContainer:'.errored',
                rules: {
                    profile:"required",
                },
                messages: {
                    profile:"Впишите номер анкеты."
                }
            });*/
            
          $(".comment li").click(function(event){
          var idVal = event.target.id;
          if (idVal == 'one'){
            $('input[name=rating]').val("1"); 
            $('#one').addClass('active');
            $('#two').removeClass('active');
            $('#three').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
          }
          if (idVal == 'two'){
            $('input[name=rating]').val("2");
            $('#one').addClass('active');
            $('#two').addClass('active');
            $('#three').removeClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
          }
          if (idVal == 'three'){
            $('input[name=rating]').val("3");
            $('#one').addClass('active');
            $('#two').addClass('active');
            $('#three').addClass('active');
            $('#four').removeClass('active');
            $('#five').removeClass('active');
          }
          if (idVal == 'four'){
            $('input[name=rating]').val("4");
            $('#one').addClass('active');
            $('#two').addClass('active');
            $('#three').addClass('active');
            $('#four').addClass('active');
            $('#five').removeClass('active');
          }
          if (idVal == 'five'){
            $('input[name=rating]').val("5");
            $('#one').addClass('active');
            $('#two').addClass('active');
            $('#three').addClass('active');
            $('#four').addClass('active');
            $('#five').addClass('active');
          }
          //alert(event.target.id);
          return false;
       }); 
       
        $('#up').click(function(){
            $('input[type=file]').click();
            return false;
        });
})