﻿<?php
/**
Template Name: Правила размещения рекламы
 */

get_header(); ?>
<div id="regulations">
	<?php /* The loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
	<?php endwhile; ?>
</div><!-- #primary -->
<?php get_footer(); ?>