﻿<?php
/**
Template Name: Добавление рекламы
 */
get_header(); 
$edit = false;
if($_POST['profile']!=''){
    $prof_id = $_POST['profile'];
    $args = array(
        'meta_query' => array(
            array(
                'key' => 'profile_id',
                'value' => $prof_id
            )
        ),
        'post_type' => 'girls',
        'posts_per_page' => 1
    );
    $posts = get_posts($args);
    $phone = get_post_meta($posts[0]->ID, 'phone', true);
    $email = get_post_meta($posts[0]->ID, 'email', true);
    $video = get_post_meta($posts[0]->ID, 'video', true);
    $title = $posts[0]->post_title;
    $content = mb_substr($posts[0]->post_content, 0, 400);
    $edit = true;
    //var_dump( $phone);
}
?>

<!--script src="<?php echo get_template_directory_uri(); ?>/js/uploader/myplupload.js"></script-->
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.min.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/vendor/jquery.ui.widget.js"></script>
 <script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
 <script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
 <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
 <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.iframe-transport.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-process.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-image.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-audio.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-video.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-validate.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/jquery.fileupload-angular.js"></script>
 <script src="<?php echo get_template_directory_uri(); ?>/js/uploader/app.js"></script>
<div id="advertising">

        <div class="advert chief">
            <?php 
                $p = get_page(243);
                echo apply_filters('the_content', $p->post_content);
            ?>  
        </div>
        <div class="advert">
            <div class="what-to-do" style="display: none">
            </div>
        <form id="fileupload" class="text-advert" action="/thankyoupage_adv" method="POST" enctype="multipart/form-data" data-ng-app="demo" data-ng-controller="DemoFileUploadController" data-file-upload="options" data-ng-class="{'fileupload-processing': processing() || loadingFiles}">
                <div class="row">
                        <input id="postPhone" type="search" value="<?php if($edit) echo $phone;?>" name="postPhone" placeholder="+___(__)___-__-__*" class="required" />
                        <?php if($edit){?><input type="hidden" value="<?php echo $posts[0]->ID;?>" name="profile_id"  /><?php } ?>
                </div>
                <div class="row">
                        <input id="postTitle" type="text" value="<?php if($edit) echo $title;?>"  name="postTitle" placeholder="Заголовок объявления*" class="required" />
                </div>
                <div class="row">
                        <input id="postEmail" type="text" value="<?php if($edit) echo $email;?>"  name="postEmail" placeholder="e-mail*" class="required" />
                </div>
                <div class="row">
                        <textarea id="postVideo" type="text" name="postVideo" cols="30" rows="2" placeholder="Ссылка на видео"><?php if($edit) echo $video;?></textarea>
                </div>
                <div class="row">
                        <textarea id="postContent" type="text" name="postContent" cols="30" rows="5" maxlength="400" placeholder="Текст объявления*"><?php if($edit) echo $content;?></textarea>
                </div>
                <div class="row">
                    <div class="input_photo">
                        <div id="dropZone" class="push-to-pass" style="margin-top:15px;">
                            <a href="#" id="up">Выберите фото или перетащите сюда (до 10 штук)*</a>
                            <input type="file" name="files[]" multiple ng-disabled="disabled" style="position:absolute; left:-1000px;" id="fileinput" />
                        </div>
                        <div class="added-photo table table-striped files ng-cloak">
                            <div class="added-photo-box" data-ng-repeat="file in queue" data-ng-class="{'processing': file.$processing()}">
                                <div data-ng-switch data-on="!!file.thumbnailUrl">
                                    <div class="preview" data-ng-switch-when="true">
                                        <a data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery><img data-ng-src="{{file.thumbnailUrl}}" alt=""></a>
                                    </div>
                                    <div class="preview" data-ng-switch-default data-file-upload-preview="file"></div>
                                </div>
                                <a href="#" onclick="return false" type="button" class="btn btn-warning cancel" data-ng-click="file.$cancel()" data-ng-hide="!file.$cancel">delete</a>
                                <!--strong data-ng-show="file.error" class="error text-danger">{{file.error}}</strong-->
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <?php if(!$edit){?>
            <div class="row">
                    <div class="captcha-holder">						
                    <script type="text/javascript"
                       src="http://www.google.com/recaptcha/api/challenge?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm">
                    </script>
                        <noscript>
                           <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LchRukSAAAAAAk69xbY7yyDDalowRvyLl5xuKmm"
                               height="300" width="500" frameborder="0"></iframe><br>
                           <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                           </textarea>
                           <input type="hidden" name="recaptcha_response_field"
                               value="manual_challenge">
                        </noscript>
                    </div>
            </div>
            <?php } ?>
            <div class="row">
                    <span class="required-fields">*Обязательные поля</span>
                    <div class="submit-holder" data-ng-click="submit()">
                        <input type="submit" value="Отправить" />
                    </div>
                </div>
        </form>
    </div>
    <div class="advert">
        <?php 
            $p = get_page(245);
            echo apply_filters('the_content', $p->post_content);
        ?>
    </div>
</div>
 <?php if ($edit){?>
<script>
    var sbmitform=$('#fileupload');
    sbmitform.append('<div id="submit-status"></div>');
    $("#fileupload").validate({
        submitHandler: function(form) {
                var statusdiv=$('#submit-status');
                statusdiv.html('<span class="ajax-load-submit"></span>');
                //form.submit();
        },
        errorLabelContainer:'.what-to-do',
        rules: {
            postTitle:"required",
            postPhone:"required",
            postContent:"required",
            postEmail:{
                required: true,
                email: true
            }
        },
        messages: {
            postTitle:"Введите заголовок объявления. ",
            postPhone:"Введите телефон. ",
            postContent:"Введите текст объявления. ",
            postEmail: {
                required: "Введите e-mail. ",
                email: "Введите правильный e-mail. "
            }
        }
        });
</script>
 <?php } else {?>
<script>
    var sbmitform=$('#fileupload');
    sbmitform.append('<div id="submit-status"></div>');
    $("#fileupload").validate({
        submitHandler: function(form) {
                var statusdiv=$('#submit-status');
                statusdiv.html('<span class="ajax-load-submit"></span>');
                form.submit();
        },
        onfocusout: false,
        onkeyup:false,
        errorLabelContainer:'.what-to-do',
        rules: {
            postTitle:"required",
            postPhone:"required",
            postContent:"required",
            postEmail:{
                required: true,
                email: true
            },
            recaptcha_response_field: {
                required: true,
                remote: { 
                    url:my_data.template_directory_uri+"/captcha/verifyCaptcha.php",
                    type:"post",
                    async:true,
                    data: {
                        recaptcha_challenge_field: function(){ return $('#recaptcha_challenge_field').val(); },
                        recaptcha_response_field: function(){ return $('#recaptcha_response_field').val(); }
                    }
                }
            }
        },
        messages: {
            postTitle:"Введите заголовок объявления. ",
            postPhone:"Введите телефон. ",
            postContent:"Введите текст объявления. ",
            postEmail: {
                required: "Введите e-mail. ",
                email: "Введите правильный e-mail. "
            },
            recaptcha_response_field: {
                required: "Введите код с картинки. ",
                remote: "Введите правильный код с картинки. "
            }
        }
        });
</script>
 <?php }?>
<?php get_footer(); ?>