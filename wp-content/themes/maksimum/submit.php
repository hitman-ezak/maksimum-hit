﻿<?php
/**
Template Name: Добавление рекламы
 */
get_header(); 
?>
<div id="advertising">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; ?>        
</div>
<?php get_footer(); ?>